<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Nums;
use Illuminate\Support\Facades\Redirect;

class Calculate extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function store(Request $request)
    {
        $exp = $request->all();
        Nums::setExp($exp);

        $request->session()->flash('hasil', Nums::getRes());
        return redirect('/');
    }
}
