<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nums
{
    private static $exp = [
        "num1" => 0,
        "num2" => 0,
        "op" => '+'
    ];
    private static $res = 0;

    public static function setExp($op)
    {
        self::$exp = $op;
    }

    public static function getRes()
    {
        return self::$res = Nums::calc();
    }

    public static function calc()
    {
        $a = (int)self::$exp['num1'];
        $b = (int)self::$exp['num2'];
        $op = self::$exp['op'];
        switch ($op) {
            case '+':
                $result =  $a + $b;
                break;
            case '-':
                $result =  $a - $b;
                break;
            case 'x':
                $result =  $a * $b;
                break;
            case '/':
                if ($b !== 0) {
                    $result =  $a / $b;
                }
                $result = 'tidak bisa dilakukan';
                break;
            default:
                $result = 'invalid';
        }
        return $result;
    }
}
