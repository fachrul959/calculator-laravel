<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Kalkulator | MVC</title>
</head>
<body>
    <div class="container mt-4">
        <h1>Kalkulator Sederhana</h1>
        <p>Masukkan Angka</p>
        
        <form action="/" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <input type="number" name="num1" class="form-control" placeholder="A" aria-label="A">
                </div>
                <div class="col">
                    <input type="number" name="num2" class="form-control" placeholder="B" aria-label="B">
                </div>
            </div>
            <div class="button-layout my-3 container">
                <div class="row">
                    <input type="submit" class="btn btn-outline-secondary col" value="+" name="op">
                    <input type="submit" class="btn btn-outline-secondary col" value="-" name="op">
                    <input type="submit" class="btn btn-outline-secondary col" value="x" name="op">
                    <input type="submit" class="btn btn-outline-secondary col" value="/" name="op">
                </div>
            </div>
        </form>
        @if(session()->has('hasil'))
            <div class="alert alert-light alert-dismissible fade show" role="alert">
                <p>Hasil = {{ session('hasil') }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>